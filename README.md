# TriStripper-rs
This repo is a port of the [original C++ TriStripper library](https://github.com/GPSnoopy/TriStripper/tree/master)
by Tanguy Fautré, for rust. Some modifications were made to make it a little more "rusty",
however it often tries to stay true to the C++ version as well. Currently, this implementation
makes use of a lot of `Rc<T>` pointers mostly because I couldn't be bothered to investigate
other strategies. So it's pretty close to being a direct port.

Please see the original readme in the original repo for more details about overall
function.

# License
The original license of the TriStripper repo is as follows, and respectively
this repo will follow the same license, with any necessary copyrights waived
to the original author as needed:

```
  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
```