use std::cmp::Ordering;

use derive_new::new;

use crate::types::Index;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, new)]
pub struct TriEdge {
    pub edge: Edge,
    pub tri_pos: usize,
}

impl Ord for TriEdge {
    fn cmp(&self, other: &Self) -> Ordering {
        // Compare `A` indices lexicographically
        match self.edge.a.cmp(&other.edge.a) {
            Ordering::Less => Ordering::Less,
            Ordering::Greater => Ordering::Greater,
            Ordering::Equal => {
                // If `A` indices are equal, compare `B` indices lexicographically
                self.edge.b.cmp(&other.edge.b)
            }
        }
    }
}

impl PartialOrd for TriEdge {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, new)]
pub struct Edge {
    pub a: Index,
    pub b: Index,
}
