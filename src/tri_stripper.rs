use std::ops::Deref;

use strum::IntoEnumIterator;

use crate::{
    cache_simulator::Cache,
    graph::{connectivity_graph::make_connectivity_graph, graph_array::GraphArray, node::Node},
    heap_array::{CompareType, HeapArray},
    policy::Policy,
    primitive::{PrimitiveGroup, PrimitiveType},
    strip::{Strip, TriOrder},
    tri::Tri,
    types::Index,
};

#[derive(Debug, Clone, PartialEq)]
pub struct TriStripper {
    primitives: Vec<PrimitiveGroup>,
    tri_graph: GraphArray<Tri>,
    tri_heap: HeapArray,
    candidates: Vec<usize>,
    cache: Cache,
    back_cache: Cache,
    strip_id: usize,
    min_strip_size: usize,
    backward_search: bool,
    first_run: bool,
}

impl TriStripper {
    /// tri_inds: Triangulated vertex indices of a mesh
    /// cache_size_kb: The size of the simulated cache in kb
    /// invert_orientation: Invert the orientation of the tris when making strips
    pub fn new(
        tri_inds: &Vec<Index>,
        cache_size_kb: Option<usize>,
        invert_orientation: bool,
    ) -> Self {
        let cache_size_kb = cache_size_kb.unwrap_or(10);
        let mut cache = Cache::new(cache_size_kb);
        cache.push_cache_hits(true);

        let num_tris = tri_inds.len() / 3;

        let mut tri_graph = GraphArray::new(num_tris);
        make_connectivity_graph(&mut tri_graph, tri_inds, invert_orientation);

        Self {
            primitives: Vec::with_capacity(64),
            // Silently ignore extra indices if (Indices.size() % 3 != 0)
            tri_graph,
            tri_heap: HeapArray::new(CompareType::LessThan, num_tris),
            candidates: Vec::with_capacity(128),
            cache,
            back_cache: Cache::new(cache_size_kb),
            strip_id: 0,
            min_strip_size: 2,
            backward_search: false,
            first_run: true,
        }
    }

    pub fn strip(&mut self, prims: &mut Vec<PrimitiveGroup>) {
        if !self.first_run {
            self.tri_graph.unmark_nodes();
            self.reset_strip_ids();
            self.cache.reset();
            self.tri_heap.clear();
            self.candidates.clear();
            self.strip_id = 0;

            // Rust note: I think this was supposed to be true?
            self.first_run = true; // false;
        }

        prims.clear();

        self.init_tri_heap();

        self.stripify();
        self.add_left_triangles();

        std::mem::swap(&mut self.primitives, prims);
    }

    pub(crate) fn init_tri_heap(&mut self) {
        // Rust note: I think I'm already doing this in a better way. If the assert fails,
        // I'm wrong
        // self.tri_heap.reserve(self.tri_graph.len());
        debug_assert!(self.tri_heap.capacity() >= self.tri_graph.len());

        // Set up the triangles priority queue
        // The lower the number of available neighbour triangles, the higher the priority.
        for tri in self.tri_graph.nodes.iter() {
            let borrow = tri.borrow();
            self.tri_heap.push(borrow.iter_len() as isize);
        }

        // We're not going to add new elements anymore
        self.tri_heap.lock();

        // Remove useless triangles
        // Note: we had to put all of them into the heap before to ensure coherency of the
        // heap_array object
        while !self.tri_heap.is_empty() && self.tri_heap.top() == 0 {
            self.tri_heap.pop();
        }
    }

    pub(crate) fn reset_strip_ids(&mut self) {
        for tri in self.tri_graph.nodes.iter_mut() {
            let mut borrow = tri.borrow_mut();
            let elem = borrow.elem.as_mut().unwrap();
            elem.reset_strip_id();
        }
    }

    pub(crate) fn stripify(&mut self) {
        while !self.tri_heap.is_empty() {
            // There is no triangle in the candidates list, refill it with the loneliest triangle
            let heap_top = self.tri_heap.position(0);
            self.candidates.push(heap_top);

            while !self.candidates.is_empty() {
                // Note: FindBestStrip empties the candidate list, while BuildStrip refills it
                let strip = self.find_best_strip();

                if strip.is_some_and(|strip| strip.size >= self.min_strip_size) {
                    self.build_strip(strip.unwrap());
                }
            }

            if !self.tri_heap.removed(heap_top) {
                self.tri_heap.erase(heap_top);
            }

            // Eliminate all the triangles that have now become useless
            while !self.tri_heap.is_empty() && self.tri_heap.top() == 0 {
                self.tri_heap.pop();
            }
        }
    }

    pub(crate) fn find_best_strip(&mut self) -> Option<Strip> {
        // Allow to restore the cache (modified by ExtendTriToStrip) and implicitly reset the cache hit count
        let cache_backup = self.cache.clone();

        let mut policy = Policy::new(self.min_strip_size, self.cache.len() != 0);

        while !self.candidates.is_empty() {
            let candidate = self.candidates.pop().unwrap();

            // Discard useless triangles from the candidate list
            let borrow = self.tri_graph.nodes[candidate].borrow();
            if borrow.marked() || self.tri_heap.get(candidate) == 0 {
                continue;
            }
            drop(borrow);

            // Try to extend the triangle in the 3 possible forward directions
            for order in TriOrder::iter() {
                let strip = self.extend_to_strip(candidate, order);
                policy.challenge(strip, self.tri_heap.get(strip.start), self.cache.hits());

                self.cache = cache_backup.clone();
            }

            // Try to extend the triangle in the 6 possible backward directions
            if self.backward_search {
                for order in TriOrder::iter() {
                    if let Some(strip) = self.back_extend_to_strip(candidate, order, false) {
                        policy.challenge(strip, self.tri_heap.get(strip.start), self.cache.hits());
                    }

                    self.cache = cache_backup.clone();
                }

                for order in TriOrder::iter() {
                    if let Some(strip) = self.back_extend_to_strip(candidate, order, true) {
                        policy.challenge(strip, self.tri_heap.get(strip.start), self.cache.hits());
                    }

                    self.cache = cache_backup.clone();
                }
            }
        }

        policy.strip
    }

    pub(crate) fn extend_to_strip(&mut self, start: usize, mut order: TriOrder) -> Strip {
        let start_order = order;

        // Begin a new strip
        self.strip_id += 1;
        let cur_strip_id = self.strip_id;
        let node = self.tri_graph.nodes[start].clone();
        let mut borrow = node.borrow_mut();
        let tri = borrow.elem.as_mut().unwrap();
        tri.strip_id = cur_strip_id;
        self.add_triangle(tri, order, false);
        drop(borrow);

        let mut size = 1;
        let mut clockwise = false;

        // Loop while we can further extend the strip
        let mut i = start;
        while i < self.tri_graph.len() && (self.cache.len() == 0 || size + 2 < self.cache.len()) {
            size += 1;

            let node = self.tri_graph.nodes[i].clone();
            let borrow = node.borrow();
            let link = self.link_to_neighbor(borrow.deref(), clockwise, &mut order, false);

            if let Some(link_ind) = link {
                let arcs = self.tri_graph.arcs.borrow();
                let link = &arcs[link_ind];
                let mut borrow = link.terminal.borrow_mut();
                let elem = borrow.elem.as_mut().unwrap();

                i = elem.index;

                elem.strip_id = cur_strip_id;
                clockwise = !clockwise;
            } else {
                size -= 1;
                i = self.tri_graph.len();
            }
        }

        Strip::new(start, start_order, size)
    }

    pub(crate) fn back_extend_to_strip(
        &mut self,
        start: usize,
        mut order: TriOrder,
        mut clockwise: bool,
    ) -> Option<Strip> {
        // Begin a new strip
        self.strip_id += 1;
        let cur_strip_id = self.strip_id;
        let node = self.tri_graph.nodes[start].clone();
        let mut borrow = node.borrow_mut();
        let tri = borrow.elem.as_mut().unwrap();
        tri.strip_id = cur_strip_id;
        self.back_add_index(tri.last_edge(order).b);
        drop(borrow);

        let mut size = 1;

        // Loop while we can further extend the strip
        let mut i = start;
        while i < self.tri_graph.len() && (self.cache.len() == 0 || size + 2 < self.cache.len()) {
            size += 1;

            let node = self.tri_graph.nodes[i].clone();
            let borrow = node.borrow();
            let link = self.back_link_to_neighbor(borrow.deref(), clockwise, &mut order);

            if let Some(link_ind) = link {
                let arcs = self.tri_graph.arcs.borrow();
                let link = &arcs[link_ind];
                let mut borrow = link.terminal.borrow_mut();
                let elem = borrow.elem.as_mut().unwrap();

                i = elem.index;

                elem.strip_id = cur_strip_id;
                clockwise = !clockwise;
            } else {
                break;
            }
        }

        // We have to start from a counterclockwise triangle.
        // Simply return an empty strip in the case where the first triangle is clockwise.
        // Even though we could discard the first triangle and start from the next counterclockwise triangle,
        // this often leads to more lonely triangles afterward.
        if clockwise {
            return None;
        }
        if self.cache.len() != 0 {
            self.cache.merge(&self.back_cache, size);
            self.back_cache.reset();
        }

        // Rust note: `i` should make sense here because it points to the current node, and
        // "begin()" would just be 0 in rust
        Some(Strip::new(i, order, size))
    }

    pub(crate) fn link_to_neighbor(
        &mut self,
        node: &Node<Tri>,
        clockwise: bool,
        order: &mut TriOrder,
        not_sim: bool,
    ) -> Option<usize> {
        let edge = node.elem.as_ref().unwrap().last_edge(*order);

        for (arc_ind, link) in node
            .arcs
            .borrow()
            .iter()
            .enumerate()
            .skip(node.begin)
            .take(node.iter_len())
        {
            // Get the reference to the possible next triangle
            let term = link.terminal.borrow();
            let tri = term.elem.as_ref().unwrap();

            // Check whether it's already been used
            if not_sim || tri.strip_id != self.strip_id {
                if !term.marked() {
                    // Does the current candidate triangle match the required position for the
                    // strip?

                    if edge.b == tri.a && edge.a == tri.b {
                        *order = if clockwise {
                            TriOrder::ABC
                        } else {
                            TriOrder::BCA
                        };
                        self.add_index(tri.c, not_sim);
                        return Some(arc_ind);
                    } else if edge.b == tri.b && edge.a == tri.c {
                        *order = if clockwise {
                            TriOrder::BCA
                        } else {
                            TriOrder::CAB
                        };
                        self.add_index(tri.a, not_sim);
                        return Some(arc_ind);
                    } else if edge.b == tri.c && edge.a == tri.a {
                        *order = if clockwise {
                            TriOrder::CAB
                        } else {
                            TriOrder::ABC
                        };
                        self.add_index(tri.b, not_sim);
                        return Some(arc_ind);
                    }
                }
            }
        }

        None
    }

    pub(crate) fn back_link_to_neighbor(
        &mut self,
        node: &Node<Tri>,
        clockwise: bool,
        order: &mut TriOrder,
    ) -> Option<usize> {
        let edge = node.elem.as_ref().unwrap().last_edge(*order);

        for (arc_ind, link) in node
            .arcs
            .borrow()
            .iter()
            .enumerate()
            .skip(node.begin)
            .take(node.iter_len())
        {
            // Get the reference to the possible next triangle
            let term = link.terminal.borrow();
            let tri = term.elem.as_ref().unwrap();

            // Check whether it's already been used
            if tri.strip_id != self.strip_id && !term.marked() {
                // Does the current candidate triangle match the required position for the
                // strip?

                if edge.b == tri.a && edge.a == tri.b {
                    *order = if clockwise {
                        TriOrder::CAB
                    } else {
                        TriOrder::BCA
                    };
                    self.back_add_index(tri.c);
                    return Some(arc_ind);
                } else if edge.b == tri.b && edge.a == tri.c {
                    *order = if clockwise {
                        TriOrder::ABC
                    } else {
                        TriOrder::CAB
                    };
                    self.back_add_index(tri.a);
                    return Some(arc_ind);
                } else if edge.b == tri.c && edge.a == tri.a {
                    *order = if clockwise {
                        TriOrder::BCA
                    } else {
                        TriOrder::ABC
                    };
                    self.back_add_index(tri.b);
                    return Some(arc_ind);
                }
            }
        }

        None
    }

    pub(crate) fn build_strip(&mut self, strip: Strip) {
        let start = strip.start;
        let mut clockwise = false;
        let mut order = strip.order;

        // Create a new strip
        self.primitives.push(PrimitiveGroup {
            indices: Vec::with_capacity(8),
            kind: PrimitiveType::TriangleStrip,
        });
        let start_node = self.tri_graph.nodes[start].clone();
        let start_borrow = start_node.borrow();
        self.add_triangle(start_borrow.elem.as_ref().unwrap(), order, true);
        drop(start_borrow);
        self.mark_tri_as_taken(start);

        // Loop while we can further extend the strip
        let mut i = start;
        for _ in 1..strip.size {
            let node = self.tri_graph.nodes[i].clone();
            let borrow = node.borrow();
            let link_ind = self
                .link_to_neighbor(borrow.deref(), clockwise, &mut order, true)
                .unwrap();

            // Rust note: We already have to unwrap it anyway
            // debug_assert!(link.is_some());

            let arcs = self.tri_graph.arcs.borrow();
            let link = &arcs[link_ind];

            let terminal = link.terminal.clone();
            drop(arcs);
            let term_borrow = terminal.borrow();
            let index = term_borrow.elem.as_ref().unwrap().index;
            drop(term_borrow);

            // Go to the next triangle
            i = index;
            self.mark_tri_as_taken(index);
            clockwise = !clockwise;
        }
    }

    pub(crate) fn add_index(&mut self, i: Index, not_sim: bool) {
        if self.cache.len() != 0 {
            self.cache.push(i, !not_sim);
        }

        if not_sim {
            self.primitives.last_mut().unwrap().indices.push(i);
        }
    }

    pub(crate) fn back_add_index(&mut self, i: Index) {
        if self.cache.len() != 0 {
            self.back_cache.push(i, true);
        }
    }

    pub(crate) fn add_triangle(&mut self, tri: &Tri, order: TriOrder, not_sim: bool) {
        match order {
            TriOrder::ABC => {
                self.add_index(tri.a, not_sim);
                self.add_index(tri.b, not_sim);
                self.add_index(tri.c, not_sim);
            }
            TriOrder::BCA => {
                self.add_index(tri.b, not_sim);
                self.add_index(tri.c, not_sim);
                self.add_index(tri.a, not_sim);
            }
            TriOrder::CAB => {
                self.add_index(tri.c, not_sim);
                self.add_index(tri.a, not_sim);
                self.add_index(tri.b, not_sim);
            }
        }
    }

    pub(crate) fn add_left_triangles(&mut self) {
        // Create the last indices array and fill it with all the triangles that couldn't be stripped
        let mut remaining = Vec::with_capacity(66);
        remaining.extend(
            self.tri_graph
                .nodes
                .iter()
                .filter_map(|node| {
                    let borrow = node.borrow();
                    if borrow.marked() {
                        None
                    } else {
                        let tri = borrow.elem.as_ref().unwrap();
                        Some([tri.a, tri.b, tri.c])
                    }
                })
                .flatten(),
        );

        // Rust note: If there is nothing to add, do nothing more
        if remaining.is_empty() {
            return;
        }

        let primitives = PrimitiveGroup {
            indices: remaining,
            kind: PrimitiveType::Triangles,
        };

        self.primitives.push(primitives);
    }

    pub(crate) fn mark_tri_as_taken(&mut self, i: usize) {
        // Mark the triangle node
        let mut borrow = self.tri_graph.nodes[i].borrow_mut();
        borrow.mark();

        let begin = borrow.begin;
        let num = borrow.iter_len();
        let arcs = borrow.arcs.clone();
        drop(borrow);

        // Remove triangle from priority queue if it isn't yet
        if !self.tri_heap.removed(i) {
            self.tri_heap.erase(i);
        }

        // Adjust the degree of available neighbour triangles
        for link in arcs.borrow().iter().skip(begin).take(num) {
            let terminal = link.terminal.borrow();
            let j = terminal.elem.as_ref().unwrap().index;
            let other = self.tri_graph.nodes[j].borrow();
            // todo replace this `other` with `terminal` if they're the same node
            if link.terminal == self.tri_graph.nodes[j] {
                log::warn!("they're the same");
            }

            if !other.marked() && !self.tri_heap.removed(j) {
                let new_degree = self.tri_heap.get(j) - 1;
                self.tri_heap.update(j, new_degree);

                // Update the candidate list if cache is enabled
                if self.cache.len() != 0 && new_degree > 0 {
                    self.candidates.push(j);
                }
            }
        }
    }
}
