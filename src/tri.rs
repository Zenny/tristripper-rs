use crate::{edge::Edge, strip::TriOrder, types::Index};
use derive_new::new;

#[derive(Debug, Copy, Clone, PartialEq, Hash, new)]
pub struct Tri {
    pub a: Index,
    pub b: Index,
    pub c: Index,
    #[new(value = "0")]
    pub strip_id: usize,
    pub index: usize,
}

impl Tri {
    pub fn reset_strip_id(&mut self) {
        self.strip_id = 0;
    }

    pub fn first_edge(&self, order: TriOrder) -> Edge {
        match order {
            TriOrder::ABC => Edge::new(self.a, self.b),
            TriOrder::BCA => Edge::new(self.b, self.c),
            TriOrder::CAB => Edge::new(self.c, self.a),
        }
    }

    pub fn last_edge(&self, order: TriOrder) -> Edge {
        match order {
            TriOrder::ABC => Edge::new(self.b, self.c),
            TriOrder::BCA => Edge::new(self.c, self.a),
            TriOrder::CAB => Edge::new(self.a, self.b),
        }
    }
}
