use crate::types::Index;

#[derive(Debug, Copy, Clone, PartialEq, Hash)]
#[repr(usize)]
pub enum PrimitiveType {
    Triangles = 0x4,     // = GL_TRIANGLES
    TriangleStrip = 0x5, // = GL_TRIANGLE_STRIP
}

#[derive(Debug, Clone, PartialEq, Hash)]
pub struct PrimitiveGroup {
    pub indices: Vec<Index>,
    pub kind: PrimitiveType,
}
