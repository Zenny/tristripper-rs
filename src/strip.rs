use derive_new::new;
use strum::EnumIter;

#[derive(Debug, Copy, Clone, PartialEq, Hash, Default, EnumIter)]
pub enum TriOrder {
    #[default]
    ABC,
    BCA,
    CAB,
}

#[derive(Debug, Copy, Clone, PartialEq, Hash, Default, new)]
pub struct Strip {
    pub start: usize,
    pub order: TriOrder,
    pub size: usize,
}
