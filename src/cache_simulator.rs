use crate::types::Index;
use std::collections::VecDeque;

#[derive(Debug, Clone, PartialEq, Hash)]
pub struct Cache {
    cache: VecDeque<Index>,
    hits: usize,
    push_hits: bool,
}

impl Cache {
    pub fn new(capacity: usize) -> Self {
        let mut cache = VecDeque::with_capacity(capacity);
        cache.resize(capacity, Index::MAX);

        Self {
            cache,
            hits: 0,
            push_hits: true,
        }
    }

    pub fn clear(&mut self) {
        self.reset_hitcount();
        self.cache.clear();
    }

    pub fn reset(&mut self) {
        self.cache.iter_mut().for_each(|x| *x = Index::MAX);
        self.reset_hitcount()
    }

    pub fn resize(&mut self, new_len: usize) {
        self.cache.resize(new_len, Index::MAX);
    }

    pub fn push_cache_hits(&mut self, enabled: bool) {
        self.push_hits = enabled;
    }

    pub fn len(&self) -> usize {
        self.cache.len()
    }

    pub fn push(&mut self, i: Index, count_hits: bool) {
        if count_hits || self.push_hits {
            if self.cache.contains(&i) {
                // Should we count the cache hits?
                if count_hits {
                    self.hits += 1;
                }

                // Should we not push the index into the cache if it's a cache hit?
                if !self.push_hits {
                    return;
                }
            }
        }

        // Manage the indices cache as a FIFO structure
        self.cache.push_front(i);
        self.cache.pop_back();
    }

    pub fn merge(&mut self, backward: &Self, possible_overlap: usize) {
        // Clamp to our len
        let overlap = possible_overlap.min(self.cache.len());

        for i in 0..overlap {
            self.push(backward.cache[i], true);
        }

        self.hits += backward.hits;
    }

    pub fn reset_hitcount(&mut self) {
        self.hits = 0;
    }

    /// hitcount()
    pub fn hits(&self) -> usize {
        self.hits
    }
}
