use crate::strip::Strip;

#[derive(Debug, Copy, Clone, PartialEq, Hash)]
pub struct Policy {
    pub strip: Option<Strip>,
    degree: isize,
    cache_hits: usize,
    min_strip_size: usize,
    cache: bool,
}

impl Policy {
    pub fn new(min_strip_size: usize, cache: bool) -> Self {
        Self {
            strip: None,
            degree: 0,
            cache_hits: 0,
            min_strip_size,
            cache,
        }
    }

    pub fn challenge(&mut self, strip: Strip, degree: isize, cache_hits: usize) {
        if strip.size < self.min_strip_size {
            log::warn!(
                "Strip size {} is lower than minimum {}",
                strip.size,
                self.min_strip_size
            );
            return;
        }

        // Cache is disabled, take the longest strip
        if !self.cache {
            if self.strip.is_none() || self.strip.is_some_and(|s| strip.size > s.size) {
                self.strip = Some(strip);
            }
        } else {
            // Cache simulator enabled
            // Priority 1: Keep the strip with the best cache hit count
            if cache_hits > self.cache_hits {
                self.strip = Some(strip);
                self.degree = degree;
                self.cache_hits = cache_hits;
            } else if cache_hits == self.cache_hits {
                if self.strip.is_some_and(|s| s.size != 0) && degree < self.degree {
                    // Priority 2: Keep the strip with the loneliest start triangle
                    self.strip = Some(strip);
                    self.degree = degree;
                } else if self.strip.is_none() || self.strip.is_some_and(|s| strip.size > s.size) {
                    // Priority 3: Keep the longest strip
                    self.strip = Some(strip);
                    self.degree = degree;
                }
            }
        }
    }
}
