use derive_new::new;

#[derive(Debug, Copy, Clone, PartialEq, Hash)]
pub enum CompareType {
    GreaterThan,
    LessThan,
}

#[derive(Debug, Copy, Clone, PartialEq, Hash, new)]
pub struct Linker {
    elem: isize,
    index: usize,
}

#[derive(Debug, Clone, PartialEq, Hash)]
pub struct HeapArray {
    heap: Vec<Linker>,
    finder: Vec<usize>,
    pub locked: bool,
    compare: CompareType,
}

impl HeapArray {
    pub fn new(compare: CompareType, capacity: usize) -> Self {
        Self {
            heap: Vec::with_capacity(capacity),
            finder: Vec::with_capacity(capacity),
            locked: false,
            compare,
        }
    }

    pub fn clear(&mut self) {
        self.heap.clear();
        self.finder.clear();
        self.locked = false;
    }

    pub fn len(&self) -> usize {
        self.heap.len()
    }

    pub fn is_empty(&self) -> bool {
        self.heap.len() == 0
    }

    pub fn removed(&self, i: usize) -> bool {
        debug_assert!(i < self.finder.len());
        self.finder[i] >= self.heap.len()
    }

    pub fn position(&self, i: usize) -> usize {
        debug_assert!(i < self.finder.len());
        self.heap[i].index
    }

    pub fn top(&self) -> isize {
        debug_assert!(!self.is_empty());
        self.heap[0].elem
    }

    /// peek() or []
    pub fn get(&self, i: usize) -> isize {
        debug_assert!(!self.removed(i));
        self.heap[self.finder[i]].elem
    }

    pub fn lock(&mut self) {
        debug_assert!(!self.locked);
        self.locked = true;
    }

    fn comp(&self, a: &Linker, b: &Linker) -> bool {
        match self.compare {
            CompareType::GreaterThan => a.elem > b.elem,
            CompareType::LessThan => a.elem < b.elem,
        }
    }

    fn swap(&mut self, a: usize, b: usize) {
        let r = self.heap[b];
        self.heap[b] = self.heap[a];
        self.heap[a] = r;

        self.finder[self.heap[a].index] = a;
        self.finder[self.heap[b].index] = b;
    }

    // Method to adjust the heap after inserting a new element.
    fn adjust(&mut self, z: usize) {
        let mut i = z;
        assert!(i < self.heap.len());

        let mut j = i;

        // Check the upper part of the heap
        while j > 0 && self.comp(&self.heap[(j - 1) / 2], &self.heap[j]) {
            self.swap(j, (j - 1) / 2);
            j = (j - 1) / 2;
        }

        // Check the lower part of the heap
        i = j;
        loop {
            j = 2 * i + 1;
            if j >= self.heap.len() {
                break;
            }

            if j + 1 < self.heap.len() && self.comp(&self.heap[j], &self.heap[j + 1]) {
                j += 1;
            }

            if self.comp(&self.heap[j], &self.heap[i]) {
                return;
            }

            self.swap(i, j);
            i = j;
        }
    }

    pub fn push(&mut self, elem: isize) -> usize {
        debug_assert!(!self.locked);

        let id = self.heap.len();
        self.finder.push(id);
        self.heap.push(Linker::new(elem, id));
        self.adjust(id);

        id
    }

    pub fn pop(&mut self) {
        debug_assert!(self.locked);
        debug_assert!(!self.is_empty());

        self.swap(0, self.heap.len() - 1);
        self.heap.pop();

        if !self.is_empty() {
            self.adjust(0);
        }
    }

    pub fn erase(&mut self, i: usize) {
        debug_assert!(self.locked);
        debug_assert!(!self.removed(i));

        let j = self.finder[i];
        self.swap(j, self.heap.len() - 1);
        self.heap.pop();

        if j != self.heap.len() {
            self.adjust(j);
        }
    }

    pub fn update(&mut self, i: usize, elem: isize) {
        debug_assert!(self.locked);
        debug_assert!(!self.removed(i));

        let j = self.finder[i];
        self.heap[j].elem = elem;
        self.adjust(j);
    }

    pub fn reserve(&mut self, size: usize) {
        self.heap.reserve(size);
        self.finder.reserve(size);
    }

    pub fn capacity(&self) -> usize {
        self.heap.capacity()
    }
}
