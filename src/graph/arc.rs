use derive_new::new;

use crate::graph::node::NodeRef;

#[derive(Debug, Clone, PartialEq, new)]
pub struct Arc<T> {
    pub terminal: NodeRef<T>,
}
