use crate::{
    edge::{Edge, TriEdge},
    graph::graph_array::GraphArray,
    tri::Tri,
    types::Index,
};

pub fn make_connectivity_graph(
    tris: &mut GraphArray<Tri>,
    tri_inds: &Vec<Index>,
    invert_orientation: bool,
) {
    debug_assert!(tris.len() == (tri_inds.len() / 3));

    // Fill the triangle data
    for (i, tri) in tris.nodes.iter().enumerate() {
        let mut borrow = tri.borrow_mut();

        let t = if invert_orientation {
            Tri::new(tri_inds[i * 3 + 2], tri_inds[i * 3 + 1], tri_inds[i * 3], i)
        } else {
            Tri::new(tri_inds[i * 3], tri_inds[i * 3 + 1], tri_inds[i * 3 + 2], i)
        };

        borrow.elem = Some(t);
    }

    // Build an edge lookup table
    let mut edge_map = Vec::with_capacity(tris.len() * 3);

    for (i, tri) in tris.nodes.iter().enumerate() {
        let borrow = tri.borrow();
        // Rust note: it will always exist as it was init above
        let tri = borrow.elem.as_ref().unwrap();
        edge_map.push(TriEdge::new(Edge::new(tri.a, tri.b), i));
        edge_map.push(TriEdge::new(Edge::new(tri.b, tri.c), i));
        edge_map.push(TriEdge::new(Edge::new(tri.c, tri.a), i));
    }

    edge_map.sort_unstable();

    // Link neighbour triangles together using the lookup table

    for i in 0..tris.nodes.len() {
        let borrow = tris.nodes[i].borrow();
        // Rust note: it will always exist as it was init above
        let tri = borrow.elem.as_ref().unwrap();

        let edge_ba = TriEdge::new(Edge::new(tri.b, tri.a), i);
        let edge_cb = TriEdge::new(Edge::new(tri.c, tri.b), i);
        let edge_ac = TriEdge::new(Edge::new(tri.a, tri.c), i);

        // Rust note: drop to use tris as mutable
        drop(borrow);

        link_neighbors(tris, &edge_map, edge_ba);
        link_neighbors(tris, &edge_map, edge_cb);
        link_neighbors(tris, &edge_map, edge_ac);
    }
}

pub fn link_neighbors(tris: &mut GraphArray<Tri>, edge_map: &Vec<TriEdge>, edge: TriEdge) {
    // Find the first edge equal to Edge
    let eq = edge_map.binary_search(&edge);

    // Rust note: If there was no equivalent edges, no work needs to be done
    if let Ok(ind) = eq {
        // See if there are any other edges that are equal
        // (if so, it means that more than 2 triangles are sharing the same edge,
        // which is unlikely but not impossible)
        let mut i = ind;
        while i < edge_map.len() && edge_map[i].edge == edge.edge {
            tris.insert_arc(edge.tri_pos, edge_map[i].tri_pos);
            i += 1;
        }
    }
}
