use std::{cell::RefCell, fmt::Debug, rc::Rc};

use crate::graph::{
    arc::Arc,
    node::{Node, NodeRef},
};

pub type ArcList<T> = Rc<RefCell<Vec<Arc<T>>>>;

#[derive(Debug, Clone, PartialEq)]
pub struct GraphArray<T: Debug> {
    pub nodes: Vec<NodeRef<T>>,
    pub arcs: ArcList<T>,
}

impl<T: Debug> GraphArray<T> {
    pub fn new(num_nodes: usize) -> Self {
        let arcs = Rc::new(RefCell::new(Vec::with_capacity(num_nodes * 3)));

        let mut nodes = Vec::with_capacity(num_nodes);
        for _ in 0..num_nodes {
            nodes.push(Node::new_ref(arcs.clone()));
        }

        Self {
            nodes,
            // triangles may have up to 3 neighbors
            arcs,
        }
    }

    pub fn empty(&self) -> bool {
        self.nodes.len() == 0
    }

    pub fn len(&self) -> usize {
        self.nodes.len()
    }

    pub fn insert_arc(&mut self, initial: usize, terminal: usize) {
        assert!(initial < self.nodes.len());
        assert!(terminal < self.nodes.len());

        let mut init_node = self.nodes[initial].borrow_mut();
        let mut arcs = self.arcs.borrow_mut();
        if init_node.iter_empty() {
            init_node.begin = arcs.len();
            init_node.end = arcs.len() + 1;
        } else {
            // we optimise here for make_connectivity_graph()
            // we know all the arcs for a given node are successively and sequentially added
            assert_eq!(init_node.end, arcs.len());

            init_node.end += 1;
        }

        let arc = Arc::new(self.nodes[terminal].clone());
        arcs.push(arc);
    }

    pub fn swap(&mut self, other: &mut GraphArray<T>) {
        std::mem::swap(self, other);
    }

    pub fn unmark_nodes(&mut self) {
        for node in self.nodes.iter_mut() {
            node.borrow_mut().unmark();
        }
    }
}
