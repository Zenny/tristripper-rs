use std::{cell::RefCell, rc::Rc};

use crate::graph::graph_array::ArcList;

pub type NodeRef<T> = Rc<RefCell<Node<T>>>;

#[derive(Debug, Clone)]
pub struct Node<T> {
    pub arcs: ArcList<T>,
    pub begin: usize,
    pub end: usize,
    pub elem: Option<T>,
    marker: bool,
}

impl<T: PartialEq> PartialEq for Node<T> {
    fn eq(&self, other: &Self) -> bool {
        self.elem == other.elem && self.begin == other.begin && self.end == other.end
    }
}

impl<T> Node<T> {
    pub fn new(arcs: ArcList<T>) -> Self {
        Self {
            arcs,
            begin: usize::MAX,
            end: usize::MAX,
            elem: None,
            marker: false,
        }
    }

    pub fn new_ref(arcs: ArcList<T>) -> NodeRef<T> {
        Rc::new(RefCell::new(Self {
            arcs,
            begin: usize::MAX,
            end: usize::MAX,
            elem: None,
            marker: false,
        }))
    }

    pub fn mark(&mut self) {
        self.marker = true;
    }

    pub fn unmark(&mut self) {
        self.marker = false;
    }

    pub fn marked(&self) -> bool {
        self.marker
    }

    /// out_empty()
    pub fn iter_empty(&self) -> bool {
        self.begin == self.end
    }

    /// out_size()
    pub fn iter_len(&self) -> usize {
        self.end - self.begin
    }

    /*#[inline]
    pub fn iter(&self) -> Take<Skip<Iter<Arc<T>>>> {
        self.arcs.borrow().iter().skip(self.begin).take(self.iter_len())
    }*/
}
